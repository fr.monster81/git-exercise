# Évaluation Git

- Prénom et nom : soupart stephen viala theo
- Commentaires : 

## Exercices

- 1 point - Fourcher et cloner le dépôt

- 1 point - Remplir votre nom ci-dessus 

- 1 point - Reprendre dans master le commit de la branche `equality_refinement` via `cherry-pick`
 git cherry-pick fbca3b123bb5735d83f9b9efa8c415024bf4edd8

- 1 point - Défaire le commit `remove unused subtract functions`
		git revert 018773291b6c6a6463f2c208209c55551e5c8912

- 1 point - Fusionner dans `master` la branche `doc/intro`
	 git merge 12af7e2d2e21d1a61fa11fe63fb23f3cdff949d4

- 2 points - Fusionner dans `master` la branche `add_distance`
ok
- 1 point - Pousser le nouveau `master`
git push origin master

- 2 points - Créer une branche sur le dernier `master`, créer une nouvelle fonction de calcul, la committer, et pousser cette nouvelle branche 
git branch FonctionCalcul, 
git switch fonctionCalcul
 (creation fichier calcul)
 git commit -m " Fonction Calcul"
git commit -m "brancheCalcul" 
git push fonctionCalcul



- 2 points - Rebaser la branche `add_tests` sur `master`
git rebase master (sur la branche add_tests)

- 1 point - Pousser la branche `add_tests` sur le dépôt distant
git push 
- 2 points - Ouvrir une merge request à partir de cette branche `add_tests` sur le dépôt source


## Questions

- Donner l’auteur du commit qui a introduit la fonction `add` dans ce projet : 
Le Père Noël

- Indiquer quelle commande utiliser, dans un projet Git,  pour changer le message du dernier commit git commit --amend -m 'votre message'

- Quelle commande, inverse de `git add`, permet de retirer un fichier du stage (prochain commit) 
git reset "Nom du fichier"


- Donner le nom du créateur de Git : Linus Torvalds


- Indiquer la(les) différence(s) entre les commandes `git init` et `git clone` 
git init permet de créé le depot distant 
git clone permet de cloner un dépot distant sur un depot local 
